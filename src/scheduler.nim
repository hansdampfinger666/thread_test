import std/deques
import std/typedthreads
import std/locks
import std/asyncdispatch
import std/httpclient
import std/options
import std/times
import os

import timer

type
  Request* = object 
    handle: int
    url: string
  Response* = object 
    handle: int
    payload: Future[string]
  Scheduler* = object
    lock*: Lock
    handle_count = 0
    queue* {.guard: lock.} = initDeque[Request]()
    response_futs* {.guard: lock.}: seq[Response]
    interrupt {.guard: lock.} = false
    watch: StopWatch
    tick_rate = initDuration(0)

var queue_thr*: Thread[ptr Scheduler]

proc init_scheduler*(tick_rate: Duration): Scheduler =
  result.tick_rate = tick_rate

proc sch_stop*(sch: var Scheduler) {.thread.} =
  {.cast(gcsafe).}:
    withLock(sch.lock):
      sch.interrupt = true

proc add_to_queue*(sch: var Scheduler, url: string): int {.thread.} =
  {.cast(gcsafe).}:
    result = sch.handle_count
    withLock sch.lock:
      sch.queue.addFirst(Request(handle: result, url: url))

proc get_response*(sch: var Scheduler, response_handle: int): Option[string] 
  {.thread.} =
  {.cast(gcsafe).}:
    withLock sch.lock:
      for idx, resp in sch.response_futs:
        if resp.handle == response_handle:
          if resp.payload.finished():
            result = some(resp.payload.read())
            sch.response_futs.delete(idx) 
            return
      result = none(string)

proc get_web_content(url: string): Future[string] {.async.} =
  let client = newAsyncHttpClient()
  defer: client.close()
  result = await client.getContent(url)

proc sch_thread*(sch: ptr Scheduler) {.thread.} =
  {.cast(gcsafe).}:
    while true:
      let time_passed = sch.watch.read()
      if (not sch.watch.running()) or (time_passed.get() >= sch.tick_rate):
        sch.watch.start()
        if sch.watch.running():
          echo "Scheduler thread tick at time: ", getTime()
      else:
        echo "scheduler thread sleeping for: ", 
          sch.tick_rate - time_passed.get()
        sleep((sch.tick_rate - time_passed.get()).inMilliseconds)
      withLock sch.lock:
        if sch.interrupt == true:
          echo "received interrupt: ", sch.interrupt
          return
        if sch.queue.len > 0:
          let req = sch.queue.popLast()
          echo "got url in queue in thread id: ", getThreadId()
          echo "firing after delay: ", sch.watch.read().get()
          sch.response_futs.add(Response(handle: req.handle, 
            payload: get_web_content(req.url)))
        for resp in sch.response_futs:
          discard waitFor(resp.payload)