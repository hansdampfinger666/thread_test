import std/terminal


proc del_lines*(lines = 1) =
  cursorUp(lines)
  eraseLine()

proc enter_cont*(delete_lines = true) =
  echo "Press Enter to continue"
  if readLine(stdin) == "\n":
    del_lines(2)