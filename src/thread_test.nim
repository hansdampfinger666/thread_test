import std/locks
import std/options
import std/strutils
import std/times

import scheduler

const base_band_url =
  "https://www.metal-archives.com/search/ajax-advanced/searching/bands/?" &
  "bandName=$1" & 
  "&genre=&country=$2" & 
  "&yearCreationFrom=&yearCreationTo=&bandNotes=&status=&themes=&location=" &
  "&bandLabelName=&sEcho=1&iColumns=3&sColumns=&" & 
  "iDisplayStart=$3" & 
  "&iDisplayLength=" &
  "&mDataProp_0=0&mDataProp_1=1&mDataProp_2=2&_=1677610047657"
const tick_rate = initDuration(seconds = 3)

proc build_test_urls(base_url: string): seq[string] =
  result.add(base_url % ["", "", "0"])
  result.add(base_url % ["", "", "200"])

proc main =
  var sch = init_scheduler(tick_rate)
  initLock(sch.lock)
  createThread(queue_thr, sch_thread, addr sch)
  echo "main thr: ", getThreadId()
  let requests = build_test_urls(base_band_url)
  var request_handles: seq[int]
  var responses: seq[string]
  for req in requests:
    request_handles.add(sch.add_to_queue(req))
  while request_handles.len > 0:
    for idx, req in request_handles:
      let response = sch.get_response(req)
      if response.isSome():
        echo "got response" 
        responses.add(response.get())
        let file_name = $responses.len & "_response.json"
        writeFile(file_name, responses[^1])
        request_handles.delete(idx)
        break
  sch.sch_stop()
  joinThread(queue_thr)
  
main()
