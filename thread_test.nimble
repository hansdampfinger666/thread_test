# Package

version       = "0.1.0"
author        = "al"
description   = "thread test"
license       = "GPL-2.0-or-later"
srcDir        = "src"
bin           = @["thread_test"]


# Dependencies

requires "nim >= 2.0.0"
